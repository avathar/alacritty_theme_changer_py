import toml
from os import listdir
from os.path import isfile, join
import os

alacritty_config_path = os.environ.get('HOME') + '/.config/alacritty/'
themes_path = alacritty_config_path + 'themes/'


def select_theme():
    # Массив строчных значений

    choose_theme = ''
    only_files = [f for f in listdir(themes_path) if isfile(join(themes_path, f))]

    # Вывод массива в консоли с индексами для выбора
    print("Выберите одно из следующих значений:")
    for index, value in enumerate(only_files):
        print(f"{index + 1}. {value[:value.rfind('.')]}")

    # Ожидание выбора пользователя
    selected_index = int(input("Введите номер выбранного значения: ")) - 1

    # Проверка корректности ввода
    if 0 <= selected_index < len(only_files):
        # Вывод выбранного значения
        item = only_files[selected_index]
        print(f"Вы выбрали: {item[:item.rfind('.')]}")
        choose_theme = item
    else:
        print("Неверный номер. Попробуйте еще раз.")
    return choose_theme


selected_theme = select_theme()
with open(alacritty_config_path + 'alacritty.toml', 'r') as config:
    data = toml.load(config)

for i, item in enumerate(data['import']):
    if not (item.find('themes') == -1):
        data['import'][i] = item[:item.rfind('/')] + '/' + selected_theme
        with open(alacritty_config_path + 'alacritty.toml', 'w') as tomlFile:
            tomlFile.write(
                toml.dumps(
                    data
                )
            )
