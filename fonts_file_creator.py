from subprocess import Popen, PIPE
import toml
import os

alacritty_config_path = os.environ.get('HOME') + '/.config/alacritty/'
path_to_alacritty_toml = alacritty_config_path + 'alacritty.toml'
themes_path = alacritty_config_path + 'themes/'
fonts_path = alacritty_config_path + 'fonts/'

fonts_list = Popen(["fc-list"], stdout=PIPE).communicate()[0]
fonts_list = str(fonts_list)[2:-3].split('\\n')
fonts_list = [x.split(":")[1] for x in fonts_list]
fonts_list = [x.split(',')[0] for x in fonts_list]
fonts_list = [x.strip() for x in fonts_list]
fonts_list = set(fonts_list)
fonts_list = list(fonts_list)
fonts_list = sorted(fonts_list)

with open('font_example.toml', 'r') as font_file:
    data = toml.load(font_file)

    for font in fonts_list:
        data['font']['bold']['family'] = font
        data['font']['bold_italic']['family'] = font
        data['font']['italic']['family'] = font
        data['font']['normal']['family'] = font
        font_file_name = font.lower().split(' ')
        font_file_name = [x.strip() for x in font_file_name]
        font_file_name = '_'.join(font_file_name)
        with open(fonts_path + font_file_name + '.toml', 'w') as tomlFile:
            tomlFile.write(
                toml.dumps(
                    data
                )
            )

# {'font': {'bold': {'family': 'VictorMono Nerd Font Mono', 'style': 'Bold'}, 'bold_italic': {'family': 'VictorMono Nerd Font Mono', 'style': 'Bold Italic'}, 'italic': {'family': 'VictorMono Nerd Font Mono', 'style': 'Italic'}, 'normal': {'family': 'VictorMono Nerd Font Mono', 'style': 'Regular'}, 'offset': {'x': 0, 'y': 0}}}
