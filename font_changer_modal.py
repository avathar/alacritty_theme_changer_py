import tkinter as tk
import toml
from os import listdir
from os.path import isfile, join
import os

alacritty_config_path = os.environ.get('HOME') + '/.config/alacritty/'
path_to_alacritty_toml = alacritty_config_path + 'alacritty.toml'
# themes_path = alacritty_config_path + 'themes/'
fonts_path = alacritty_config_path + 'fonts/'


def on_select(event):
    # Получение индекса выбранного элемента
    index = listbox.curselection()[0]

    # Получение текста выбранного элемента
    selected_value = listbox.get(index)

    # selected_theme = select_theme()
    with open(path_to_alacritty_toml, 'r') as config:
        data = toml.load(config)

    flag = False
    for i, item in enumerate(data['import']):
        if item.find('fonts') != -1:
            data['import'][i] = item[:item.rfind('/')] + '/' + str(selected_value)
            flag = True
    if not flag:
        data['import'].append(fonts_path + str(selected_value))
    with open(path_to_alacritty_toml, 'w') as tomlFile:
        tomlFile.write(
            toml.dumps(
                data
            )
        )

    # Вывод выбранного элемента
    result_label.config(text=f"Вы выбрали: {selected_value}")


# Инициализация Tkinter
root = tk.Tk()
root.title("Выбор элемента")

# Создание Listbox
listbox = tk.Listbox(root, selectmode=tk.SINGLE)
listbox.pack(pady=20)

# Добавление элементов в Listbox
string_array = sorted([f for f in listdir(fonts_path) if isfile(join(fonts_path, f))])
for item in string_array:
    listbox.insert(tk.END, item)

# Привязка события выбора элемента в Listbox
listbox.bind('<<ListboxSelect>>', on_select)

# Добавление метки для вывода результата
result_label = tk.Label(root, text="")
result_label.pack(pady=10)

# Запуск Tkinter
root.mainloop()
