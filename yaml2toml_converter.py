import yaml
import toml
from os import listdir
from os.path import isfile, join

themes_path = './alacritty-theme/themes/'

only_files = [f for f in listdir(themes_path) if isfile(join(themes_path, f))]

for filename in only_files:
    dotIndex = filename.rfind('.')
    name = filename[:dotIndex]

    with open(themes_path + filename, 'r') as file:
        data = yaml.safe_load(file)
        with open('themes/' + name + '.toml', 'w') as tomlFile:
            tomlFile.write(
                toml.dumps(
                    data
                )
            )
