# alacritty_theme_changer_py



## Getting started

[yaml2toml_converter.py](yaml2toml_converter.py) Convert yaml to toml theme files for alacritty

[theme_changer.py](theme_changer.py) Change theme from terminal

[theme_changer_modal.py](theme_changer_modal.py) Change theme from modal window

[fonts_file_creator.py](fonts_file_creator.py) Create fonts configs in /fonts folder

[font_changer_modal.py](font_changer_modal.py) Change font in modal window

[theme_changer_modal2.py](theme_changer_modal2.py) Other variant of modal window theme changer