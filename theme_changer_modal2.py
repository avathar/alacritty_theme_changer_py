import tkinter as tk
import toml
from os import listdir
from os.path import isfile, join
import os
from math import ceil

alacritty_config_path = os.environ.get('HOME') + '/.config/alacritty/'
path_to_alacritty_toml = alacritty_config_path + 'alacritty.toml'
themes_path = alacritty_config_path + 'themes/'


def on_select(text):
    with open(path_to_alacritty_toml, 'r') as config:
        data = toml.load(config)
    flag = False
    for i, item in enumerate(data['import']):
        if item.find('themes') != -1:
            data['import'][i] = item[:item.rfind('/')] + '/' + str(text)
            flag = True
    if not flag:
        data['import'].append(themes_path + str(text))
    with open(path_to_alacritty_toml, 'w') as tomlFile:
        tomlFile.write(
            toml.dumps(
                data
            )
        )

    # Вывод выбранного элемента
    result_label.config(text=f"Вы выбрали: {str(text)}")


def create_button(item, r, i, columns):
    btn = tk.Button(root, command=lambda: on_select(item), text="{}".format(item))
    btn.grid(row=r, column=i % columns)


# Инициализация Tkinter
root = tk.Tk()
root.title("Выбор темы")
# root.geometry("300x300")
# frm = tk.Frame(root)
# frm.grid()

columns = 4
# Добавление элементов в Listbox
string_array = [f for f in listdir(themes_path) if isfile(join(themes_path, f))]
string_array = list(sorted(string_array))

row_count = ceil(len(string_array) / columns)

for r in range(row_count):
    for i, item in enumerate(string_array[r * columns:r * columns + columns], r * columns):
        create_button(item, r, i, columns)

# Добавление метки для вывода результата
result_label = tk.Label(root, text="")
# result_label.pack(pady=10)

# Запуск Tkinter
root.mainloop()
